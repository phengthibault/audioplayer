package fr.cpe.audioplayer.adapters;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;

import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.io.InputStream;

import java.util.List;

import fr.cpe.audioplayer.R;
import fr.cpe.audioplayer.databinding.TopAlbumsItemBinding;
import fr.cpe.audioplayer.models.TopAlbums;
import fr.cpe.audioplayer.viewModel.TopAlbumsViewModel;


public class TopAlbumsListAdapter extends RecyclerView.Adapter<TopAlbumsListAdapter.ViewHolder> {
    List<TopAlbums> topAlbumsList;

    public TopAlbumsListAdapter(List<TopAlbums> albumsList) {
        assert albumsList != null;
        topAlbumsList = albumsList;


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        TopAlbumsItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.top_albums_item, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        TopAlbums topAlbums = topAlbumsList.get(position);

        holder.viewModel.setTopAlbumsViewModel(topAlbums);
        holder.addImage();

    }

    @Override
    public int getItemCount() {
        return topAlbumsList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TopAlbumsItemBinding binding;
        private TopAlbumsViewModel viewModel = new TopAlbumsViewModel();

        ViewHolder(TopAlbumsItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            this.binding.setTopAlbumsViewModel(viewModel);

        }

        void addImage(){
            ImageView image = this.binding.imageView;
            DownloadImageTask downloadImageTask = new DownloadImageTask(image);
            String url = binding.getTopAlbumsViewModel().getURL();
            downloadImageTask.execute(binding.getTopAlbumsViewModel().getURL());
        }
    }

    /*  Gérer l'affichage des images (pochette chanson) */
    private static class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];

            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
