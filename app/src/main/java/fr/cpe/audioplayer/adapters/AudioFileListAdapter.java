package fr.cpe.audioplayer.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import fr.cpe.audioplayer.fragments.SongPlayerFragment;

import fr.cpe.audioplayer.interfaces.SongPlayerListener;
import fr.cpe.audioplayer.models.AudioFile;
import fr.cpe.audioplayer.R;
import fr.cpe.audioplayer.databinding.AudioFileItemBinding;
import fr.cpe.audioplayer.viewModel.AudioFileViewModel;


public class AudioFileListAdapter extends RecyclerView.Adapter<AudioFileListAdapter.ViewHolder> {

    List<AudioFile> audioFileList;
    private SongPlayerFragment songPlayerFragment;

    private SongPlayerListener songPlayerListener;
    public void setMyListener(SongPlayerListener listener){
        this.songPlayerListener = listener;

    }

    public AudioFileListAdapter(List<AudioFile> fileList) {
        assert fileList != null;
        audioFileList = fileList;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AudioFileItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.audio_file_item, parent, false);

        return new ViewHolder(binding,this.songPlayerListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        AudioFile file = audioFileList.get(position);

        holder.viewModel.setAudioFile(file);
    }



    @Override
    public int getItemCount() {
        return audioFileList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private AudioFileItemBinding binding;
        public SongPlayerListener songPlayerListener;
        private AudioFileViewModel viewModel = new AudioFileViewModel();

        ViewHolder(AudioFileItemBinding binding, SongPlayerListener songPlayerListener) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.setAudioFileViewModel(viewModel);
            this.songPlayerListener = songPlayerListener;

            this.binding.getRoot().setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    AudioFile titleClick=viewModel.getAudioFile();
                    ViewHolder.this.songPlayerListener.onClickSong(titleClick);
                }
            });
        }
    }
}