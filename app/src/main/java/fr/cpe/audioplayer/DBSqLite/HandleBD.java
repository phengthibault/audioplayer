package fr.cpe.audioplayer.DBSqLite;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import fr.cpe.audioplayer.models.TopAlbums;

public class HandleBD {

    private SQLiteDatabase database;

    public HandleBD(SQLiteDatabase database){
        this.database=database;
    }
    private String[] allColumns = { MySQLiteHelper.COLUMN_ID,
            MySQLiteHelper.COLUMN_ARTISTES, MySQLiteHelper.COLUMN_ALBUMS, MySQLiteHelper.COLUMN_URL};


    public List<TopAlbums> getAllData() {
        List<TopAlbums> listTopAlbums = new ArrayList();
        Cursor cursor = database.query(MySQLiteHelper.TABLE_FM,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            listTopAlbums.add(new TopAlbums(cursor.getString(cursor.getColumnIndex(allColumns[1])),
                    cursor.getString( cursor.getColumnIndex(allColumns[2])),
                    cursor.getString( cursor.getColumnIndex(allColumns[3]))));
            cursor.moveToNext();
        }
        cursor.close();
        return listTopAlbums;
    }
    public void insertInBD(String id ,TopAlbums topAlbums) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_ARTISTES,topAlbums.getArtist());
      //  values.put(MySQLiteHelper.COLUMN_ID, id);
        values.put(MySQLiteHelper.COLUMN_ALBUMS,topAlbums.getTrack());
        values.put(MySQLiteHelper.COLUMN_URL,topAlbums.getURL());

        long insertId = database.insert(MySQLiteHelper.TABLE_FM,null,values);
        if (insertId == -1) {
           System.out.println("erreur dans l'insertion");
        }

    }



}
