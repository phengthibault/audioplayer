package fr.cpe.audioplayer.DBSqLite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteHelper extends SQLiteOpenHelper {

    public static final String TABLE_FM = "FM";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_ARTISTES = "artistes";
    public static final String COLUMN_URL= "genjjjjres";
    //public static final String COLUMN_URL = "url";
    public static final String COLUMN_ALBUMS = "albums";

    private static final String DATABASE_NAME = "FM.db";
    private static final int DATABASE_VERSION = 3;

    // Commande sql pour la création de la base de données
    private static final String DATABASE_CREATE = "create table "
            + TABLE_FM + "(" + COLUMN_ID
            + " integer primary key autoincrement, " + COLUMN_ARTISTES+ " TEXT, "
            + COLUMN_URL +" TEXT, "+COLUMN_ALBUMS
            + " text not null);";

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(MySQLiteHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FM);
        onCreate(db);
    }
}