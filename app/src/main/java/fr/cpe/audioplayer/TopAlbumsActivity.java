package fr.cpe.audioplayer;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import fr.cpe.audioplayer.DBSqLite.MySQLiteHelper;
import fr.cpe.audioplayer.Services.FmService;
import fr.cpe.audioplayer.databinding.ActivityTopAlbumsBinding;
import fr.cpe.audioplayer.fragments.TopAlbumsFragment;

public class TopAlbumsActivity extends AppCompatActivity {
    private ActivityTopAlbumsBinding binding;
    private FmService fmService;
    private MySQLiteHelper dbHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbHelper = new MySQLiteHelper(this);
        FmService fmService=new FmService(dbHelper);
        fmService.execute(); //execute asynctask object this will resolve NetworkOnMainThreadExcection

        binding = DataBindingUtil.setContentView(this, R.layout.activity_top_albums);

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        final TopAlbumsFragment fragmentTopAlbums= new TopAlbumsFragment(fmService);
        transaction.replace(R.id.fragment_topAlbums,fragmentTopAlbums);
        transaction.commit();

    }
}

