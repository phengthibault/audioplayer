package fr.cpe.audioplayer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import fr.cpe.audioplayer.Services.PlayerService;
import fr.cpe.audioplayer.databinding.ActivityMainBinding;
import fr.cpe.audioplayer.fragments.AudioFileListFragment;
import fr.cpe.audioplayer.fragments.SongPlayerFragment;
import fr.cpe.audioplayer.interfaces.ActionButtons;
import fr.cpe.audioplayer.interfaces.SongPlayerListener;
import fr.cpe.audioplayer.models.AudioFile;


public class MainActivity extends AppCompatActivity implements AudioFileListFragment.OnCompleteListener {
    private ActivityMainBinding binding;
    private SongPlayerFragment fragmentSongPlayer;
    private AudioFileListFragment fragmentAudioFileList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        makeActionWithPermission();

    }

    public void showStartup() {
        /* Inititialition des fragments */

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        this.fragmentSongPlayer = new SongPlayerFragment();
        transaction.replace(R.id.fragment_songPlayer, fragmentSongPlayer);

        this.fragmentAudioFileList = new AudioFileListFragment();
        transaction.replace(R.id.fragment_container, fragmentAudioFileList);

        final PlayerService playerService = new PlayerService();


        /*  Listener permettant de lancer une musique lorsqu'on clique dessus */
        SongPlayerListener listener = new SongPlayerListener() {
            @Override
            public void onClickSong(AudioFile audioFile) {  //audiofile
                fragmentSongPlayer.updateTitle(audioFile);

                if (playerService.getMediaPlayerExist() == true) {
                    playerService.stop();
                } else {
                }

                playerService.init(audioFile.getFilePath());
                playerService.play();

            }
        };
        fragmentAudioFileList.setMyListener(listener);



        /*  Listener gérant les différents boutons du player */
        ActionButtons actionButtonsListener = new ActionButtons() {
            @Override
            public void onClickActionButton(String title, String action) {

                AudioFile nextSong = fragmentAudioFileList.actionsButtons(title, action);
                fragmentSongPlayer.updateTitle(nextSong);

                if (playerService.getMediaPlayerExist() == false) {
                    playerService.init(nextSong.getFilePath());
                }

                switch (action) {
                    case "pause":
                        playerService.pause();
                        break;
                    case "play":
                        playerService.play();
                        break;
                    default:
                        playerService.stop();
                        playerService.init(nextSong.getFilePath());
                        playerService.play();
                }
            }

        };
        fragmentSongPlayer.setMyListener(actionButtonsListener);

        /*  Listener permettant de lancer la muisque suivante à la fin de la musique courante */
        playerService.getMediaPlayer().setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                String currentTitle = fragmentSongPlayer.getTitle();

                AudioFile nextSong = fragmentAudioFileList.actionsButtons(currentTitle, "next");
                fragmentSongPlayer.updateTitle(nextSong);
                playerService.stop();
                playerService.init(nextSong.getFilePath());
                playerService.play();
            }
        });


        transaction.commit();

    }


    /* Demander les permissions à l'utilisateur */
    final static int MY_PERMISSION_REQUEST_CODE = 1; // valeur arbitraire
    final static String MY_PERMISSION_NAME = Manifest.permission.READ_EXTERNAL_STORAGE/*...*/;

    public void makeActionWithPermission() {
        // A-t-on la permission de le faire ?
        if (ActivityCompat.checkSelfPermission(MainActivity.this, MY_PERMISSION_NAME) != PackageManager.PERMISSION_GRANTED) {
            // Si "non", alors il faut demander la ou les permissions

            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{MY_PERMISSION_NAME},
                    MY_PERMISSION_REQUEST_CODE);
            // Finir ici le traitement : ne pas bloquer.
            // On attend la réponse via onRequestPermissionsResult
        } else {
            showStartup();
        }
    }

    /* Action à effectuer en fonction du résultat de la demande de permission */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // On relance la méthode de réalisation de l'action. Cette fois, on a l'autorisation.
                    showStartup();
                } else {
                    // l'activité ne démarre pas
                }
                return;
            }
        }
    }

    /* Bouton pour accèder à l'activité TopAlbums" */
    public void clickToTopAlbums(View v) {
        Intent secondeActivite = new Intent(MainActivity.this, TopAlbumsActivity.class);
        startActivity(secondeActivite);
    }

    /* Initilialise le lecteur avec la première musique de la liste quand le fragment AudioFileList est chargé */
   public void onComplete() {
         this.fragmentSongPlayer.updateTitle(this.fragmentAudioFileList.getList().get(0));
    }
}