package fr.cpe.audioplayer.models;

public class TopAlbums {
    private String artist;
    private String track;
    private String URL;

    public TopAlbums( ) {

    }

    public TopAlbums(String artist, String track, String URL) {
        this.artist = artist;
        this.track = track;
        this.URL = URL;
    }


    public String getArtist() {
        return artist;
    }

    public String getTrack() {
        return track;
    }

    public String getURL() {
        return URL;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void setTrack(String album) {
        this.track = album;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }
}
