package fr.cpe.audioplayer.viewModel;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import fr.cpe.audioplayer.models.TopAlbums;

public class TopAlbumsViewModel  extends BaseObservable {
    private TopAlbums topAlbums = new TopAlbums();
    private String track;

    public TopAlbumsViewModel(){

    }
    public TopAlbumsViewModel(TopAlbums track){
        this.track=track.getTrack();
    }

    public void setTopAlbumsViewModel(TopAlbums file) {
        topAlbums = file;
        notifyChange();
    }

    @Bindable
    public String getArtist() {
        return topAlbums.getArtist();
    }

    @Bindable
    public String getURL() {
        return topAlbums.getURL();
    }

    @Bindable
    public String getTrack() {
        return topAlbums.getTrack();
    }

}
