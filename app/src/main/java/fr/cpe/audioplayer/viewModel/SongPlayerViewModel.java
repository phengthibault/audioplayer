package fr.cpe.audioplayer.viewModel;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import fr.cpe.audioplayer.models.AudioFile;

public class SongPlayerViewModel  extends BaseObservable {
    private AudioFile audioFile = new AudioFile();
    private String title;
    public SongPlayerViewModel(AudioFile file){
        this.title=file.getTitle();
    }
    public void setSongPlayerViewModel(AudioFile file) {
        audioFile = file;
        notifyChange();
    }

    @Bindable
    public String getArtist() {
        return audioFile.getArtist();
    }

    @Bindable
    public String getTitle() {
        return audioFile.getTitle();
    }

    @Bindable
    public String getAlbum() {
        return audioFile.getAlbum();
    }

    @Bindable
    public String getDuration() {
        return audioFile.getDurationText();
    }
}
