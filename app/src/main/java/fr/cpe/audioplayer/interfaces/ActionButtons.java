package fr.cpe.audioplayer.interfaces;

import java.io.IOException;

import fr.cpe.audioplayer.models.AudioFile;

//public interface NextPreviousSong {
public interface ActionButtons {

    public void onClickActionButton(String title, String action);

}
