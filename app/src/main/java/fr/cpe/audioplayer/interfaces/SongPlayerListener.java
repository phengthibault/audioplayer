package fr.cpe.audioplayer.interfaces;

import fr.cpe.audioplayer.models.AudioFile;

public interface SongPlayerListener {

    public void onClickSong(AudioFile audio);

}
