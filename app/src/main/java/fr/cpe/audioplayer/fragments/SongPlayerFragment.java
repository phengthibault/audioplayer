package fr.cpe.audioplayer.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;


import fr.cpe.audioplayer.R;
import fr.cpe.audioplayer.databinding.SongPlayerFragmentBinding;
import fr.cpe.audioplayer.interfaces.ActionButtons;
import fr.cpe.audioplayer.models.AudioFile;
import fr.cpe.audioplayer.viewModel.AudioFileViewModel;

public class SongPlayerFragment extends Fragment {
    private AudioFileViewModel viewModel;
    private SongPlayerFragmentBinding binding;
    private ActionButtons actionButtonsListener;

    public void setMyListener(ActionButtons actionButtonsListener) {
        this.actionButtonsListener=actionButtonsListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {


        binding = DataBindingUtil.inflate(inflater,
                R.layout.song_player_fragment, container, false);

        /* Initialization song player*/

        binding.titleChoose.setText("");

        this.actionsButtonsListener();

        return binding.getRoot();

    }

    public void updateTitle(AudioFile audioFile){
       binding.titleChoose.setText(audioFile.getTitle());
    }

    public String getTitle(){
        return  binding.titleChoose.getText().toString();
    }

    public void actionsButtonsListener(){

        binding.nextbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String action  = "next";
                actionButtonsListener.onClickActionButton( binding.titleChoose.getText().toString(),action);
            }
        });

        binding.previousbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String action  = "previous";
                actionButtonsListener.onClickActionButton(binding.titleChoose.getText().toString(),action);

            }
        });
        binding.playbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String action  = "play";
                actionButtonsListener.onClickActionButton(binding.titleChoose.getText().toString(),action);

            }
        });
        binding.pausebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String action  = "pause";
                actionButtonsListener.onClickActionButton(binding.titleChoose.getText().toString(),action);

            }
        });

    }
}



