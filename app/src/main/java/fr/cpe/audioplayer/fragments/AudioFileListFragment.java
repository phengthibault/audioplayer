package fr.cpe.audioplayer.fragments;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import fr.cpe.audioplayer.R;
import fr.cpe.audioplayer.adapters.AudioFileListAdapter;
import fr.cpe.audioplayer.databinding.AudioFileListFragmentBinding;
import fr.cpe.audioplayer.interfaces.SongPlayerListener;
import fr.cpe.audioplayer.models.AudioFile;

public class AudioFileListFragment extends Fragment {
    private TextView title = null;
    private AudioFileListAdapter adapter = null;
    private AudioFileListFragmentBinding binding;



    private List<AudioFile> list;

    public AudioFileListFragment() {

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {



        binding = DataBindingUtil.inflate(inflater,
                R.layout.audio_file_list_fragment, container, false);

        binding.audioFileList.setLayoutManager(new LinearLayoutManager(binding.getRoot().getContext()));

        /*Get audio files path from the phone*/
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI; // La carte SD
        String[] projection = {
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.TRACK,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.YEAR}; //chemin du fichier, titre, artist
        Cursor cursor = this.getContext().getContentResolver().query(uri,projection,null,null,null);

            /* Save audio files in our list*/
        List<AudioFile> audioList = new ArrayList<>();
        cursor.moveToFirst();

        /* Ajouter les musiques du téléphones à notre liste d'AudioFile */
        try {
            do {
                String data = cursor.getString(cursor.getColumnIndex("title"));
                audioList.add(new AudioFile(
                        cursor.getString(cursor.getColumnIndex("title")),
                        cursor.getString(cursor.getColumnIndex("artist")),
                        cursor.getString(cursor.getColumnIndex("album")),
                        cursor.getInt(cursor.getColumnIndex("year")),
                        cursor.getInt(cursor.getColumnIndex("duration")),
                        cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA))
                ));
            } while (cursor.moveToNext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        cursor.close();

        list = audioList;

        /* Move data to our adapter */
        this.adapter = new AudioFileListAdapter(audioList);
        binding.audioFileList.setAdapter(this.adapter);
        this.adapter.setMyListener(this.songPlayerListener);

        mListener.onComplete();

        return binding.getRoot();

    }

    private SongPlayerListener songPlayerListener;
    public void setMyListener(SongPlayerListener listener)
    {
        songPlayerListener = listener;

    }

    /* Récupérer le bon fichier audio en fonction du bouton pressé */
    public AudioFile actionsButtons(String title, String action) {
        AudioFile audio=null;
        for (AudioFile audiofile : this.list) {
            if (audiofile.getTitle().equals(title)) {

                int index = list.indexOf(audiofile);
                audio = audiofile;

                switch (action) {
                    case "next":
                        System.out.println(list.size());
                        System.out.println(index);
                        if (index <list.size()-1) {
                            audio = list.get(index + 1);
                        }else{
                            audio = list.get(0);
                        }
                        break;
                    case "previous":
                        if (index!=0) {
                            audio = list.get(index - 1);
                        }else{
                            audio = list.get(list.size()-1);
                        }
                        break;
                    case "play":audio=audiofile;
                        break;
                    case "pause":audio=audiofile;
                        break;
                    default:audio=audiofile;
                    System.out.println(audio.getTitle());

                }

            }
        }
        return audio;
    }
    public List<AudioFile> getList() {
        return list;
    }


    /* Listener permettant de notifier la main activity que le fragment est complètement créé */
    public static interface OnCompleteListener {
        public abstract void onComplete();
    }

    private OnCompleteListener mListener;

    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            this.mListener = (OnCompleteListener)context;
        }
        catch (final ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnCompleteListener");
        }
    }


}

