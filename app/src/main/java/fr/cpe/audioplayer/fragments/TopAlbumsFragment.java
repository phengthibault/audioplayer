package fr.cpe.audioplayer.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.List;

import fr.cpe.audioplayer.DBSqLite.HandleBD;
import fr.cpe.audioplayer.DBSqLite.MySQLiteHelper;
import fr.cpe.audioplayer.R;
import fr.cpe.audioplayer.Services.FmService;
import fr.cpe.audioplayer.adapters.TopAlbumsListAdapter;
import fr.cpe.audioplayer.databinding.TopAlbumsListFragmentBinding;
import fr.cpe.audioplayer.models.TopAlbums;

public class TopAlbumsFragment extends Fragment {
    private TopAlbumsListAdapter adapter = null;
    private TopAlbumsListFragmentBinding binding;
    private List<TopAlbums> topAlbumsList;
    private MySQLiteHelper dbHelper;
    private FmService fmService;
    private HandleBD handleBD;

    public TopAlbumsFragment(FmService fmService) {
        this.fmService = fmService;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {


        binding = DataBindingUtil.inflate(inflater,
                R.layout.top_albums_list_fragment, container, false);

        binding.topAlbumsList.setLayoutManager(new LinearLayoutManager(binding.getRoot().getContext()));

        List<TopAlbums> topAlbumsListfromDB = fmService.getListTopAlbums();

        /* Move data to our adapter */
        this.adapter = new TopAlbumsListAdapter(topAlbumsListfromDB);
        binding.topAlbumsList.setAdapter(this.adapter);

        return binding.getRoot();

    }

}
