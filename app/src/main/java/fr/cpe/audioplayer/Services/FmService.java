package fr.cpe.audioplayer.Services;

import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import fr.cpe.audioplayer.DBSqLite.HandleBD;
import fr.cpe.audioplayer.DBSqLite.MySQLiteHelper;
import fr.cpe.audioplayer.models.TopAlbums;

public class FmService extends AsyncTask<String, Void, String> {
    //API key	a1b6ff548b37373a75224c8d64a5eecf
    // Shared secret	e6654bbdfb2995976cdb7fae2958af51
    //Password Audioplayer1-
    //username thibault.pheng@cpe.fr

    URL url;
    HttpURLConnection c;
    JSONObject json = new JSONObject();
    String YOUR_API_KEY = "a1b6ff548b37373a75224c8d64a5eecf";
    String data=null;
    private List<TopAlbums> listTopAlbums=new ArrayList<>();
    private MySQLiteHelper dbHelper;
    private HandleBD handleBD;
    private SQLiteDatabase database;

    //http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&artist=Cher&api_key=YOUR_API_KEY
    //http://ws.audioscrobbler.com/2.0/?method=artist.gettopalbums&artist=cher&api_key=a1b6ff548b37373a75224c8d64a5eecf&format=json
    public FmService( MySQLiteHelper dbHelper) {
        this.dbHelper=dbHelper;
    }

    @Override
    protected String doInBackground(String... strings) {

        try {
            url = new URL("http://ws.audioscrobbler.com/2.0/?method=chart.gettoptracks&api_key="+YOUR_API_KEY+"&format=json");
            c = (HttpURLConnection) url.openConnection();
            c.setRequestMethod("GET");
            c.setDoInput(true);
            c.setRequestProperty("Accept", "∗/∗");
            InputStream istream = c.getInputStream();
            InputStreamReader isr = new InputStreamReader(istream);
            BufferedReader br = new BufferedReader(isr);
             this.data = br.readLine();
            istream.close();

            String jsonString=this.data;
            JSONObject rootObj = new JSONObject(jsonString);
            JSONObject albumsObject = rootObj.getJSONObject("tracks");
            JSONArray albums= albumsObject.getJSONArray("track");

            String artist=null,track=null,URL=null;
         for (int i=0; i<albums.length(); i++) {
                JSONObject song = albums.getJSONObject(i);
             track=song.getString("name");
             artist=song.getJSONObject("artist").getString("name");
             URL = song.getJSONArray("image").getJSONObject(3).getString("#text");

             System.out.println("URL:"+URL);
             TopAlbums topAlbums=new TopAlbums(artist,track,URL);
             this.listTopAlbums.add(topAlbums);
         }
            createDB();

        } catch (java.io.IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return this.data;
    }

public void createDB(){
    try {
        database = this.dbHelper.getWritableDatabase();
        database.beginTransaction();
        handleBD=new HandleBD(database);
        int i=1;
        for (TopAlbums topAlbums:listTopAlbums){
            handleBD.insertInBD(String.valueOf(i),topAlbums);
            i++;
        }
        database.setTransactionSuccessful();

        database.endTransaction();
    }catch (Exception e) {
        // how to do the rollback
        e.printStackTrace();

    }
}

    public String getData(){
        return this.data;
    }



    public List<TopAlbums> getListTopAlbums() {
        return this.listTopAlbums;
    }


}




