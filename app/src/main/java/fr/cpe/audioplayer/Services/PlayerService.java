package fr.cpe.audioplayer.Services;


import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;

import androidx.annotation.Nullable;

import java.io.IOException;

public class PlayerService extends Service implements MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener {
        private final Binder binder = new PlayerBinder();
        private MediaPlayer mediaPlayer = new MediaPlayer();
        private boolean mediaPlayerExist = false;

        public int onStartCommand(Intent intent, int flags, int startId) {
            return START_STICKY_COMPATIBILITY;
        }

        public void init(String path){
            try {
                mediaPlayer.setDataSource(path);
            } catch (IOException e) {
                // e.printStackTrace();
                System.out.println("error+"+e);
            }
            try {
                mediaPlayer.prepare();
            } catch (IOException e) {
                // e.printStackTrace();
                System.out.println("error2+"+e.getMessage());
            }
            mediaPlayerExist = true;

        }

        public void play()  {
            mediaPlayer.start();
            onCompletion(mediaPlayer);
        }

        public boolean getMediaPlayerExist(){
            return mediaPlayerExist;
        }

        public void stop() {
            this.mediaPlayer.stop();
            this.mediaPlayer.reset();
            mediaPlayerExist = false;

        }
        public void pause() {
            this.mediaPlayer.pause();

        }

    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    @Nullable
        @Override
        public IBinder onBind(Intent intent) {
            return binder;
        }
        public void onPrepared(MediaPlayer player) {
        }
        @Override
        public boolean onError(MediaPlayer mp, int what, int extra) {
            return false;
        }

        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {

        }


        public class PlayerBinder extends Binder {
                public PlayerService getService() {
                    return PlayerService.this;
                }
            }
}
